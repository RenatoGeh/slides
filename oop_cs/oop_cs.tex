%&tex

\documentclass{beamer}

\usepackage[portuguese]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage[singlelinecheck=false]{caption}
\usepackage{minted}
\usepackage{xcolor}

\uselanguage{Portuguese}
\languagepath{Portuguese}

\setbeamertemplate{theorems}[numbered]

\usemintedstyle[c]{perldoc}

\newcommand{\code}[1]{\lstinline[mathescape=true]{#1}}
\newcommand{\mcode}[1]{\lstinline[mathescape]!#1!}

\newcommand{\algorithmautorefname}{Algorithm}
\algrenewcommand\algorithmicrequire{\textbf{Entrada}}
\algrenewcommand\algorithmicensure{\textbf{Saída}}
\algrenewcommand\algorithmicif{\textbf{se}}
\algrenewcommand\algorithmicthen{\textbf{então}}
\algrenewcommand\algorithmicelse{\textbf{senão}}
\algrenewcommand\algorithmicfor{\textbf{para todo}}
\algrenewcommand\algorithmicdo{\textbf{faça}}
\algnewcommand{\LineComment}[1]{\State\,\(\triangleright\) #1}

\newcommand{\defeq}{\vcentcolon=}

\title{\Huge Orientação a Objetos em C:\\ Um Estudo de Caso}
\subtitle{Git e Kernel Linux}
\date{}
\author{Matheus Tavares \texttt{<matheus.bernardino@usp.br>}\\
Renato Lui Geh \texttt{<renatogeh@gmail.com>}}

% If \titlelogo is not set, macaw does not add a title logo to the title page.
\newcommand{\titlelogo}{imgs/logo.png}

\usetheme{macaw}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introdução}

\begin{frame}
  \frametitle{\LARGE Técnicas de OO em linguagens não OO}

  \textbf{Motivação:} Usar técnicas boas mas ausentes na linguagem para
  complementá-la ao resolver um problema.
  \vfill

  \begin{quote}
    No programming technique solves all problems.\newline
    No programming language produces only correct results.
    \vskip5mm
    \hspace*\fill{\small--- Schreiner, Axel T (1993). Object-Oriented Programming With ANSI-C }
  \end{quote}
\end{frame}

\begin{frame}
  \frametitle{Conteúdo}

  Veremos os seguintes conceitos implementados em C:\\

  \begin{itemize}
    \item Atributos privados
    \item Métodos privados
    \item Herança pública
    \item Herança privada
    \item Herança múltipla
    \item Classes abstratas e Polimorfismo
    \item Metaprogramação
    \item Design Pattern: Iterator
  \end{itemize}\vspace{0.5cm}

  Em particular, veremos OOP em C no \textbf{Git} e \textbf{Kernel Linux IIO}.
\end{frame}

\section{Kernel Linux IIO}

\begin{frame}
  \frametitle{Linux IIO}

  \small
  \begin{itemize}
    \item \mintinline{c}{iio_dev}: Industrial Input/Output Device
    \item \mintinline{c}{ad7780_state}: Analog to Digital Sigma-Delta Converter Device
  \end{itemize}\vspace{0.5cm}

  \begin{figure}
    \centering
    \tikzset{every picture/.style={line width=0.75pt}}
    \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
      \draw   (110.25,139.72) -- (210.25,139.72) -- (210.25,210.5) -- (110.25,210.5) -- cycle ;
      \draw   (120.25,168.45) .. controls (120.25,163.92) and (123.92,160.25) .. (128.45,160.25) -- (192.05,160.25) .. controls (196.58,160.25) and (200.25,163.92) .. (200.25,168.45) -- (200.25,193.05) .. controls (200.25,197.58) and (196.58,201.25) .. (192.05,201.25) -- (128.45,201.25) .. controls (123.92,201.25) and (120.25,197.58) .. (120.25,193.05) -- cycle ;
      \draw (160.89,147.44) node [scale=0.8] [align=left] {{\fontfamily{pcr}\selectfont iio\_dev}};
      \draw (160.5,170) node [scale=0.7] [align=left] {{\fontfamily{pcr}\selectfont ad7780\_state}};
    \end{tikzpicture}
  \end{figure}\vspace{0.5cm}

  \begin{itemize}
    \item \mintinline{c}{struct ad7780_state} especialização de \mintinline{c}{struct iio_dev}.
    \item Em outras palavras, \mintinline{c}{ad7780_state} é subclasse de \mintinline{c}{iio_dev}.
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Herança}

  \textbf{Herança por composição}
  \vfill

  Seja $S$ uma classe, e $C$ subclasse de $S$. Assume-se que $S$ e $C$ têm tamanhos $n$ e $m$ bytes
  de memória em \textit{fields}. Cria-se um objeto do tipo $C$ da seguinte forma:\\~\\

  \begin{enumerate}
    \item Aloque um bloco $B$ de $n+m+a$ bytes;
    \item Reserve os $n$ primeiros para $S$;
    \item Reserve o intervalo $[n+a,n+a+m]$ ($m$ bytes) para $C$;
    \item Retorne um ponteiro para $B$ do tipo $S$.
  \end{enumerate}~\\

  Desta forma, $C$ é um objeto ``privado'' de $S$.
\end{frame}

\begin{frame}
  \frametitle{No Kernel}

  \begin{block}{Definições}
    \vspace{-0.5cm}
    \begin{flalign*}
      \quad S &\defeq \text{\mintinline{c}{iio_dev}}&&\\
      C &\defeq \text{\mintinline{c}{ad7780_state}}\\
      n &\defeq \text{\mintinline{c}{sizeof(struct iio_dev)}}\\
      m &\defeq \text{\mintinline{c}{sizeof(struct ad7780_state)}}
    \end{flalign*}
  \end{block}\vfill

  Função \mintinline{c}{devm_iio_device_alloc} aloca o bloco $B$ e retorna um ponteiro
  \mintinline{c}{struct iio_dev*} para o começo do bloco.\vfill

  \begin{itemize}
    \small
    \item \mintinline{c}{drivers/iio/adc/ad7780.c:ad7780_state}
    \item \mintinline{c}{include/linux/iio/iio.h:iio_dev}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Acesso à subclasse}

  Mas como acessar o \textit{field} $c$ de $s$ a partir do endereço do bloco $B$?
  \vfill

  \begin{minted}{c}
#define ALIGN(x, a) ALIGN_MASK(x, (typeof(x))(a)-1)
#define ALIGN_MASK(x, mask) (((x) + (mask)) & ~(mask))
#define ALIGN_BYTES CACHE_BYTES_IN_POWER_OF_2

static inline void *priv(const struct S *s) {
  /* Retorna ponteiro para objeto c "escondido" em s. */
  return (char*) s + ALIGN(sizeof(struct S), ALIGN_BYTES);
}
  \end{minted}
  \vfill

  \begin{itemize}
    \item \mintinline{c}{include/linux/kernel.h:ALIGN}
    \item \mintinline{c}{include/linux/iio/iio.h:iio_priv}
    \item \mintinline{c}{include/uapi/linux/kernel.h:__ALIGN_KERNEL_MASK}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Entendendo \mintinline{c}{ALIGN}}

  Note que \mintinline{c}{ALIGN} é alguma função do tamanho de $S$ e alguma potência de dois.\\

  \begin{block}{Lembre-se que...}
    Acesso de potências de dois em memória é mais rápido.
  \end{block}\vspace{0.5cm}

  Queremos acessar uma porção da memória alocada do bloco de memória perto de
  \mintinline{c}{s+sizeof(S)}.  \textcolor{gray}{(desenho na lousa)}\vspace{0.5cm}

  \begin{alertblock}{Objetivo}
    Mostrar que \mintinline{c}{ALIGN(x, a)} é o \textbf{menor múltiplo de \mintinline{c}{a} maior
    que \mintinline{c}{x}}.
  \end{alertblock}
\end{frame}

\begin{frame}
  \begin{lemma}
    Seja $n,m\in\mathbb{Z}_k$, tal que $m=2^c-1$ para algum $c\in\mathbb{N}, c<k$. Então:

    \begin{equation*}
      (n+m) \text{ \& } \sim(m) \mid 2^c.
    \end{equation*}

    \label{multiple}
  \end{lemma}

  \textbf{Demonstração.}\\

  Note primeiro que $m=2^c-1=(0\ldots 01\ldots 1)_2$, então $(\sim m)=(1\ldots 10\ldots 0)_2$.
  Então $(n+m)\text{ \& }\sim(m)$ são todos os bits mais significativos de $n+2^c-1$ a partir do
  $c$-ésimo bit. Mas isso corresponde exatamente a tomar $n+m$ e ``subtrair'' todos os bits menos
  significativos a partir de $c-1$. Mais formalmente,

  \begin{equation*}
    (n+m)\text{ \& }\sim(m)=(n+m)-((n+m)\text{ \& }(2^c-1)).
  \end{equation*}

\end{frame}

\begin{frame}
  Mas a parcela da direita pode ser reescrita como

  \begin{equation*}
    ((n+m)\text{ \& }(2^c-1)) \equiv (n+m)\mod 2^c.
  \end{equation*}

  Em outras palavras, tomar os $c-1$ bits menos significativos é equivalente ao resto da divisão
  por $2^c$. Para visualizar isso, lembre que todos os bits $2^i$, com $i>c$ são múltiplos de
  $2^c$, e portanto são equivalentes a zero. Disso, temos que
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+m)-\left((n+m)\mod 2^c\right)\\
                            &= (n+2^c-1)-((n+2^c-1)\mod 2^c)\\
                            &= (n+2^c-1)-((n-1)\mod 2^c).
  \end{align*}
  Agora note que, se $n<2^c$, então $(n+2^c-1)-n+1=2^c\mid 2^c$ e portanto a hipótese é
  trivialmente verdadeira. O mesmo pode ser dito quando $n=2^c$. Tome então $n>2^c$ e analizemos
  dois possíveis casos.
\end{frame}

\begin{frame}
  \textbf{Caso 1:} $2^c\mid n$
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((n-1)\mod 2^c)\\
                            &= (n+2^c-1)+1\\
                            &= (n+2^c)\mid 2^c.\text{\color{gray}\quad(por suposição)}
  \end{align*}

  \textbf{Caso 2:} $2^c\nmid n$
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((n-1)\mod 2^c)\\
                            &= (n-r+r+2^c-1)-((r-1)\mod 2^c),
  \end{align*}

  onde $r=n\mod 2^c$, ou seja, o ``resto'' de $n/2^c$.
\end{frame}

\begin{frame}
  Então temos dois subcasos: quando $0\equiv r-1\mod 2^c$, e portanto
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((r-1)\mod 2^c)\\
                            &= (n-r+r+2^c-1)\\
                            &= ((n-r)+(r-1)+2^c)\mid 2^c,
  \end{align*}
  E finalmente quando $0\not\equiv r-1\mod 2^c$. Neste subcaso, como $r<2^c$, $(r-1\mod 2^c)=r-1$
  então:
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((r-1)\mod 2^c)\\
                            &= (n-r+r+2^c-1)-r+1\\
                            &= ((n-r)+2^c)\mid 2^c.
  \end{align*}

  Portanto $(n+m)\text{ \& }\sim(m)\mid 2^c$.
  \qed
\end{frame}

\begin{frame}
  \begin{lemma}
    Seja $n,m\in\mathbb{Z}_k$, tal que $m=2^c-1$ para algum $c\in\mathbb{N}, c<k$. Então:

    \begin{equation*}
      t=(n+m) \text{ \& } \sim(m)
    \end{equation*}

    É o menor múltiplo de $2^c$ maior que $n$.\label{minimum}
  \end{lemma}

  \textbf{Demonstração.}

  Mostraremos exaustivamente que $t$ é de fato o candidato múltiplo de $2^c$ mínimo com respeito
  a $n$. Recordemos dos casos expostos na demonstração do Lema \ref{multiple}. Quando $n< 2^c$,
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((n-1)\mod 2^c)\\
                            &= n+2^c-1-n+1\\
                            &= 2^c = t
  \end{align*}
  e portanto $t$ é o menor múltiplo de $2^c$ maior que $n$.
\end{frame}

\begin{frame}
  Para $n=2^c$,
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((n-1)\mod 2^c)\\
                            &= 2^c+2^c-1+1\\
                            &= 2^c+2^c = t,
  \end{align*}
  e novamente $t$ é o múltiplo ``mínimo''. Recordemos dos dois casos $n>2^c$.\\

  \textbf{Caso 1:} $2^c\mid n$
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((n-1)\mod 2^c)\\
                            &= (n+2^c-1)+1\\
                            &= n+2^c = t
  \end{align*}
  Se $n$ é múltiplo de $2^c$, então o próximo múltiplo maior que $n$ é $n+2^c$.
\end{frame}

\begin{frame}
  \textbf{Caso 2:} $2^c\nmid n$\\

  Quando $0\equiv r-1\mod 2^c$, $r=1$, já que $r<2^c$ por definição.
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((r-1)\mod 2^c)\\
                            &= n-1+2^c = t
  \end{align*}
  Mas se $r=1$, então $n-1$ é múltiplo de $2^c$, e portanto o menor múltiplo maior que $n$ é
  $n-1+2^c$.\\

  Para o último subcaso, tome $0\not\equiv r-1\mod 2^c$. Então
  \begin{align*}
    (n+m)\text{ \& }\sim(m) &= (n+2^c-1)-((r-1)\mod 2^c)\\
                            &= (n-r+r+2^c-1)-r+1\\
                            &= n-r+2^c. = t
  \end{align*}
  Novamente, $n-r$ é múltiplo de $2^c$ por definição, e portanto o próximo candidato é $n-r+2^c$.
  \qed
\end{frame}

\begin{frame}
  \begin{theorem}
    A função \textnormal{\mintinline{c}{ALIGN(sizeof(struct S), ALIGN_BYTES)}} retorna o menor
    endereço de memória múltiplo de \textnormal{\mintinline{c}{ALIGN_BYTES}} maior que
    \textnormal{\mintinline{c}{sizeof(struct S)}}.
  \end{theorem}
  \vfill

  \textbf{Demonstração.}\\

  Segue direto pelo Lema \ref{multiple} e Lema \ref{minimum}.

  \qed
\end{frame}

\begin{frame}[fragile]
  \frametitle{Usando \mintinline{c}{ALIGN}}

  \begin{minted}{c}
static int ad7780_probe(struct spi_device *spi) {
  struct ad7780_state *st;
  struct iio_dev *indio_dev;

  indio_dev = devm_iio_device_alloc(&spi->dev, sizeof(*st));
  if (!indio_dev) return -ENOMEM;

  st = iio_priv(indio_dev);
  st->gain = 1;
  ...
}
  \end{minted}

  \vspace{0.5cm}
  \begin{itemize}
    \item \mintinline{c}{drivers/iio/adc/ad7780.c:ad7780_probe}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Múltipla herança}

  \small
  \begin{itemize}
    \item \mintinline{c}{ad7780_state} subclasse de \mintinline{c}{iio_dev} (\textbf{herança});
    \item \mintinline{c}{ad7780_state} subclasse de \mintinline{c}{ad_sigma_delta}
      (\textbf{composição}).
  \end{itemize}
  \normalsize
  \vfill

  \begin{figure}
    \centering
    \tikzset{every picture/.style={line width=0.75pt}}
    \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
      \draw   (110.25,139.72) -- (210.25,139.72) -- (210.25,210.5) -- (110.25,210.5) -- cycle ;
      \draw    (160.25,240.25) -- (160.25,192.75) ;
      \draw [shift={(160.25,190.75)}, rotate = 450] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
      \draw   (110.75,241) -- (209.75,241) -- (209.75,319.75) -- (110.75,319.75) -- cycle ;
      \draw   (119.5,278.45) .. controls (119.5,273.92) and (123.17,270.25) .. (127.7,270.25) -- (192.05,270.25) .. controls (196.58,270.25) and (200.25,273.92) .. (200.25,278.45) -- (200.25,303.05) .. controls (200.25,307.58) and (196.58,311.25) .. (192.05,311.25) -- (127.7,311.25) .. controls (123.17,311.25) and (119.5,307.58) .. (119.5,303.05) -- cycle ;
      \draw   (120.25,168.45) .. controls (120.25,163.92) and (123.92,160.25) .. (128.45,160.25) -- (192.05,160.25) .. controls (196.58,160.25) and (200.25,163.92) .. (200.25,168.45) -- (200.25,193.05) .. controls (200.25,197.58) and (196.58,201.25) .. (192.05,201.25) -- (128.45,201.25) .. controls (123.92,201.25) and (120.25,197.58) .. (120.25,193.05) -- cycle ;
      \draw (160.89,147.44) node [scale=0.8] [align=left] {{\fontfamily{pcr}\selectfont iio\_dev}};
      \draw (160.5,251) node [scale=0.7] [align=left] {{\fontfamily{pcr}\selectfont ad7780\_state}};
      \draw (160.25,280.38) node  [align=left] {{\fontfamily{pcr}\selectfont {\tiny ad\_sigma\_delta}}};
    \end{tikzpicture}
  \end{figure}
  \vfill

  No fundo, ambos são composição, mas de jeitos diferentes. :)
\end{frame}

\begin{frame}
  \frametitle{Herança ``pública'' vs ``privada''}
  \small

  \begin{block}{Herança privada}
    \small
    Vimos herança ``privada'' com \mintinline{c}{iio_dev} e \mintinline{c}{ad7780_state}.
    \begin{itemize}
      \item Atributos de subclasse são privados;
      \item Herança em tempo de execução;
      \item Subclasse pode ser \mintinline{c}{ad7780_state}, \mintinline{c}{ad7793_state},
        \mintinline{c}{mcp3422}, etc.
    \end{itemize}
  \end{block}

  Agora veremos herança ``pública''.

  \begin{block}{Herança pública}
    Veremos herança pública com \mintinline{c}{ad_sigma_delta} e \mintinline{c}{ad7780_state}.
    \begin{itemize}
      \item Atributos da superclasse e subclasse são públicos;
      \item Herança em tempo de compilação;
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]
  \begin{description}
    \item[\mintinline{c}{ad_sigma_delta}:] Conversor Analógico-Digital Sigma-Delta
    \item[\mintinline{c}{ad7780_state}:] Conversor ADSD para AD7170/1 e AD7780/1
  \end{description}\vspace{0.2cm}

  \begin{minted}{c}
struct ad7780_state {
  const struct ad7780_chip_info *chip_info;
  struct regulator *reg;
  struct gpio_desc *powerdown_gpio;
  ...
  unsigned int int_vref_mv;

  struct ad_sigma_delta sd;
}
  \end{minted}

  \vspace{0.1cm}
  Em herança privada, a \textbf{superclasse (\mintinline{c}{iio_dev}) contém a subclasse
  (\mintinline{c}{ad7780_state})}.\\

  Em herança pública, a \textbf{subclasse (\mintinline{c}{ad7780_state}) contém a superclasse
  (\mintinline{c}{ad_sigma_delta})}.
\end{frame}

\begin{frame}[fragile]
  \frametitle{Acesso à subclasse}

  Como acessar o objeto $c$ da subclasse $C$ quando o objeto $s$ da superclasse $S$ está
  \emph{dentro} de $C$?\\

  \begin{minted}[fontsize=\small]{c}
#define container_of(ptr, type, member) \
  (type*)((void*)(ptr) - ((size_t)&((type*)0)->member)

static struct ad7780_state *ad_sigma_delta_to_ad7780(
  struct ad_sigma_delta *sd) {
    return container_of(sd, struct ad7780_state, sd);
}
  \end{minted}

  \vspace{0.5cm}
  \begin{itemize}
    \small
    \item \mintinline{c}{drivers/iio/adc/ad7780.c:ad_sigma_delta_to_ad7780}
    \item \mintinline{c}{include/linux/kernel.h:container_of}
    \item \mintinline{c}{include/linux/stddef.h:offsetof}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Entendendo \mintinline{c}{container_of}}

  Queremos acessar o ponteiro ``de fora'', ou seja, achar o \mintinline{c}{struct ad7780_state*}
  que contém \mintinline{c}{struct ad_sigma_delta*}.\\

  \begin{minted}[fontsize=\small]{c}
#define container_of(ptr, type, member) \
  (type*)((void*)(ptr) - ((size_t)&((type*)0)->member)

static struct ad7780_state *ad_sigma_delta_to_ad7780(
  struct ad_sigma_delta *sd) {
    return container_of(sd, struct ad7780_state, sd);
}
  \end{minted}

  \vspace{0.3cm}
  \begin{block}{Truque}
    \mintinline{c}{&((type*)0)->member}: retorna qual seria o endereço de \mintinline{c}{member} se
    \mintinline{c}{type*} estivesse no endereço zero. Ou seja, qual o tamanho em bytes até
    \mintinline{c}{member}. \textcolor{gray}{(desenho na lousa)}
  \end{block}
\end{frame}

\section{Git}

\begin{frame}[fragile]
  \frametitle{Objeto \mintinline{c}{dir-iterator}}

  Exemplo de uso (simplificado):
  \vfill

  \begin{minted}{c}
struct dir_iterator *iter = dir_iterator_begin(path);

while (dir_iterator_advance(iter) == ITER_OK) {
  if (want_to_stop_iteration()) {
    dir_iterator_abort(iter);
    break;
  }

  // Access information about the current path:
  if (S_ISDIR(iter->st.st_mode))
    printf("%s is a directory\n", iter->relative_path);
}
  \end{minted}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A API \mintinline{c}{dir-iterator.h}}
  \begin{minted}{c}
// The current iteration state, with path,
// basename and etc.
struct dir_iterator {
  struct strbuf path;
  const char *relative_path;
  const char *basename;
  struct stat st;
};

struct dir_iterator *dir_iterator_begin(const char *path);
int dir_iterator_advance(struct dir_iterator *iterator);
int dir_iterator_abort(struct dir_iterator *iterator);
  \end{minted}

  \vspace{0.5cm}
  \begin{itemize}
    \item \mintinline{c}{dir-iterator.h}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\mintinline{c}{dir-iterator.c}: construtor}

  %\begin{minted}[fontsize=\small]{c}
%struct dir_iterator_int {
  %struct dir_iterator base;

  %size_t levels_nr;
  %size_t levels_alloc;
  %struct dir_iterator_level *levels;
  %unsigned int flags;
%};

%struct dir_iterator *dir_iterator_begin(const char *path) {
  %struct dir_iterator_int *iter = xcalloc(1, sizeof(*iter));
  %struct dir_iterator *dir_iterator = &iter->base;
  %... /* Initialize fields. */
  %return dir_iterator;
%}
  %\end{minted}
  \begin{figure}
    \centering\includegraphics[width=1.05\linewidth]{imgs/dir-iterator-initialization.png}
  \end{figure}

  \begin{itemize}
    \item \mintinline{c}{dir-iterator.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Como acessar os atributos privados?}

  \begin{minted}[fontsize=\small]{c}
int dir_iterator_advance(struct dir_iterator *dir_iterator)
{
	struct dir_iterator_int *iter =
		(struct dir_iterator_int *)dir_iterator;
	// Use iter as needed
	...
}
  \end{minted}

  \begin{itemize}
    \item \mintinline{c}{dir-iterator.c}
    \item Nota: poderiamos ter ``métodos privados'' aqui com \mintinline{c}{static}.
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Membros privados: como funciona}
  \begin{figure}
    \centering\includegraphics[width=1\linewidth]{imgs/dir-iterator-memory.png}
  \end{figure}

  \begin{itemize}
    \item Use essa técnica com cautela:
    \begin{itemize}
      \item \mintinline{c}{memcpy} e outros:
      {\scriptsize \mintinline{c}{sizeof(struct dir_iterator) != sizeof(struct dir_iterator_int)}}
      \item vetores e inicialização fora de {\scriptsize \mintinline{c}{dir_iterator_begin()}}
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Classes abstratas, herança e polimorfismo}


  \begin{itemize}
    \item \mintinline{c}{refs/refs-internal.h}
    \item \mintinline{c}{refs/iterator.c}
    \item \mintinline{c}{refs/files-backend.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\textit{Big picture}}
  \begin{figure}
    \centering\includegraphics[width=1.05\linewidth]{imgs/iterator-uml.png}
  \end{figure}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A classe abstrata \mintinline{c}{ref_iterator}}

  \begin{minted}[fontsize=\small]{c}
struct ref_iterator {
	struct ref_iterator_vtable *vtable;

	unsigned int ordered : 1;
	const char *refname;
	const struct object_id *oid;
	unsigned int flags;
};
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{refs/refs-internal.h}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{\mintinline{c}{ref_iterator}: métodos abstratos}

  \begin{minted}[fontsize=\small]{c}
int ref_iterator_advance(struct ref_iterator *ref_iterator)
{
	return ref_iterator->vtable->advance(ref_iterator);
}

int ref_iterator_abort(struct ref_iterator *ref_iterator)
{
	return ref_iterator->vtable->abort(ref_iterator);
}
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{refs/iterator.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A sub-classe \mintinline{c}{reflog_iterator}}

  \begin{minted}[fontsize=\scriptsize]{c}
static struct ref_iterator *reflog_iterator_begin(struct ref_store *
                                                  ref_store,
						  const char *gitdir)
{
	struct files_reflog_iterator *iter = xcalloc(1, sizeof(*iter));
	struct ref_iterator *ref_iterator = &iter->base;
	struct strbuf sb = STRBUF_INIT;

	base_ref_iterator_init(ref_iterator,
                               &files_reflog_iterator_vtable, 0);
	strbuf_addf(&sb, "%s/logs", gitdir);
	iter->dir_iterator = dir_iterator_begin(sb.buf);
	iter->ref_store = ref_store;
	strbuf_release(&sb);

	return ref_iterator;
}

  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{refs/files-backend.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Herança múltipla}

  \begin{minted}[fontsize=\scriptsize]{c}
static int files_reflog_iterator_advance(struct ref_iterator *ref_iterator)
{
	struct files_reflog_iterator *iter =
		(struct files_reflog_iterator *)ref_iterator;
	struct dir_iterator *diter = iter->dir_iterator;
	int ok;

	while ((ok = dir_iterator_advance(diter)) == ITER_OK) {
		int flags;
        ...
}
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{refs/files-backend.c}
  \end{itemize}
\end{frame}

\section{Metaprogramação no Git}

\begin{frame}[fragile]
  \frametitle{Metaprogramação}

  \begin{minted}[fontsize=\small]{c}
#include "cache.h"
...

define_commit_slab(blame_suspects, struct blame_origin *);
static struct blame_suspects blame_suspects;
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{blame.c}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Metaprogramação}

  \begin{minted}[fontsize=\small]{c}
#define define_commit_slab(slabname, elemtype)   \
	declare_commit_slab(slabname, elemtype); \
	implement_static_commit_slab(slabname, elemtype)
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{commit-slab.h}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Metaprogramação}

  \begin{minted}[fontsize=\small]{c}
#define declare_commit_slab(slabname, elemtype) \
                                                \
struct slabname {                               \
	unsigned slab_size;                     \
	unsigned stride;                        \
	unsigned slab_count;                    \
	elemtype **slab;                        \
}
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{commit-slab-decl.h}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Metaprogramação}

  \begin{minted}[fontsize=\scriptsize,tabsize=8,obeytabs]{c}
#define implement_static_commit_slab(slabname, elemtype) \
	implement_commit_slab(slabname, elemtype, MAYBE_UNUSED static)

#define implement_commit_slab(slabname, elemtype, scope)       \
                                                               \
scope void init_ ##slabname## _with_stride(struct slabname *s, \
                                           unsigned stride)    \
{                                                              \
	unsigned int elem_size;                                \
	if (!stride)                                           \
		stride = 1;                                    \
		...                                            \
}                                                              \
...
  \end{minted}

  \vfill
  \begin{itemize}
    \item \mintinline{c}{commit-slab-impl.h}
  \end{itemize}
\end{frame}

\section{Dúvidas?}

\begin{frame}[allowframebreaks]
  \frametitle{Referências}
  \bibliographystyle{plainurl}
  \bibliography{references.bib}
  \nocite{*}
\end{frame}

\end{document}
